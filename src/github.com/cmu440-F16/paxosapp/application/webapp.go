package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/cmu440-F16/paxosapp/rpc/paxosrpc"
	"net/http"
	"net/rpc"
	"net/url"
	"strconv"
	"text/template"
)

var (
	rows       int
	cols       int
	client     *rpc.Client
	deadNode   bool
	numRetries int
)

var (
	nodePort = flag.Int("nodePort", 9019, "paxosnode port #")
	myport   = flag.Int("myport", 8080, "port # to listen on")
)

func handler(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("index.html")
	t.Execute(w, "hello")

	u, _ := url.Parse(r.URL.String())
	v := u.Query()
	if len(v) != 0 {
		row := v["row"][0]
		col := v["col"][0]
		draw := v["draw"][0]
		proposePixel(row+","+col, draw)
	}

}

func deadNodeHandler(w http.ResponseWriter, r *http.Request) {
	if deadNode == true {
		fmt.Printf("this node is dead %s\n", strconv.Itoa(*nodePort))
		fmt.Fprintf(w, "true")
	}
}

func refreshHandler(w http.ResponseWriter, r *http.Request) {
	//Call get values for each row, for each col
	m := make(map[string]string)
	for row := 0; row < rows; row++ {
		for col := 0; col < cols; col++ {
			key := strconv.Itoa(row) + "," + strconv.Itoa(col)
			getValueArgs := paxosrpc.GetValueArgs{Key: key}
			var getValueReply paxosrpc.GetValueReply

			err := client.Call("PaxosNode.GetValue", getValueArgs, &getValueReply)
			if err != nil {
				return
			}
			if getValueReply.Status == paxosrpc.KeyFound {
				m[key] = (getValueReply.V).(string)
			} else {
				m[key] = "ffffff"
			}

		}
	}

	data, _ := json.Marshal(m)
	fmt.Fprintf(w, "%s", data)
}

func proposePixel(key string, colored string) error {
	var proposeNumArg paxosrpc.ProposalNumberArgs
	proposeNumArg = paxosrpc.ProposalNumberArgs{Key: key}
	var proposeNumReply paxosrpc.ProposalNumberReply
	err := client.Call("PaxosNode.GetNextProposalNumber", proposeNumArg, &proposeNumReply)


	for i := 0; i < numRetries; i++ {
		err = client.Call("PaxosNode.GetNextProposalNumber", proposeNumArg, &proposeNumReply)
		if err == nil {
			break
		}
	}
	if err != nil {
		return err
	}
	n := proposeNumReply.N
	proposeArgs := paxosrpc.ProposeArgs{N: n, Key: key, V: colored}
	var proposeReply paxosrpc.ProposeReply

	//Give up proposing this pixel if failed after numRetries
	err = client.Call("PaxosNode.Propose", proposeArgs, &proposeReply)
	for i := 0; i < numRetries; i++ {
		err = client.Call("PaxosNode.Propose", proposeArgs, &proposeReply)
		if err == nil {
			break
		}
	}
	if err != nil {
		return err
	}
	if proposeReply.NoMajority == true {
		deadNode = true
	}
	if proposeReply.V.(string) != "ffffff" {
		fmt.Printf("proposal reply is %s\n", proposeReply.V.(string))
	}
	return nil

}

func jsHandler(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("paxosapp.js")
	t.Execute(w, "hello")
}

func cssHandler(w http.ResponseWriter, r *http.Request) {
	t, _ := template.ParseFiles("styles.css")
	t.Execute(w, "hello")
}

func main() {
	rows = 40
	cols = 90
	numRetries = 5
	flag.Parse()
	cli, err := rpc.DialHTTP("tcp", "localhost:"+strconv.Itoa(*nodePort))

	for err != nil {
		cli, err = rpc.DialHTTP("tcp", "localhost:"+strconv.Itoa(*nodePort))
	}
	client = cli
	//Initialize all pixels as white at the beginning
	for row := 0; row < rows; row++ {
		for col := 0; col < cols; col++ {
			key := strconv.Itoa(row) + "," + strconv.Itoa(col)
			proposePixel(key, "ffffff")
		}
	}

	fmt.Printf("clientNode on paxos:%v is up, listening on port:%v \n", strconv.Itoa(*nodePort), strconv.Itoa(*myport))
	http.HandleFunc("/", handler) // handler on initial landing page
	http.HandleFunc("/deadNode", deadNodeHandler)
	http.HandleFunc("/paxosapp.js", jsHandler)
	http.HandleFunc("/styles.css", cssHandler)
	http.HandleFunc("/refresh", refreshHandler)
	http.ListenAndServe(":"+strconv.Itoa(*myport), nil) // webserver running access http://localhost:8080/
}
