#!/bin/bash

if [ -z $GOPATH ]; then
    echo "FAIL: GOPATH environment variable is not set"
    exit 1
fi

if [ -n "$(go version | grep 'darwin/amd64')" ]; then    
    GOOS="darwin_amd64"
elif [ -n "$(go version | grep 'linux/amd64')" ]; then
    GOOS="linux_amd64"
else
    echo "FAIL: only 64-bit Mac OS X and Linux operating systems are supported"
    exit 1
fi

# Build the student's paxos node implementation.
# Exit immediately if there was a compile-time error.
go install github.com/cmu440-F16/paxosapp/runners/prunner
if [ $? -ne 0 ]; then
   echo "FAIL: code does not compile"
   exit $?
fi

# Build the test binary to use to test the student's paxos node implementation.
# Exit immediately if there was a compile-time error.
go install github.com/cmu440-F16/paxosapp/application
if [ $? -ne 0 ]; then
   echo "FAIL: code does not compile"
   exit $?
fi


# Pick random ports between [10000, 20000).
NODE_PORT0=$(((RANDOM % 10000) + 10000))
NODE_PORT1=$(((RANDOM % 10000) + 10000))
NODE_PORT2=$(((RANDOM % 10000) + 10000))
NODE_PORT3=$(((RANDOM % 10000) + 10000))
NODE_PORT4=$(((RANDOM % 10000) + 10000))
NODE_PORT5=$(((RANDOM % 10000) + 10000))
NODE_PORT6=$(((RANDOM % 10000) + 10000))
NODE_PORT7=$(((RANDOM % 10000) + 10000))
NODE_PORT8=$(((RANDOM % 10000) + 10000))
NODE_PORT9=$(((RANDOM % 10000) + 10000))
PAXOS_APP=$GOPATH/bin/application
PAXOS_NODE=$GOPATH/bin/prunner
ALL_PORTS="${NODE_PORT0},${NODE_PORT1},${NODE_PORT2},${NODE_PORT3},${NODE_PORT4},${NODE_PORT5},${NODE_PORT6},${NODE_PORT7},${NODE_PORT8},${NODE_PORT9}"

echo ${ALL_PORTS}
##################################################

# Start paxos node.
${PAXOS_NODE} -myport=${NODE_PORT0} -ports=${ALL_PORTS} -N=10 -id=0 2>&1 & # /dev/null &
PAXOS_NODE_PID0=$!
${PAXOS_NODE} -myport=${NODE_PORT1} -ports=${ALL_PORTS} -N=10 -id=1 2>&1 & # /dev/null &
PAXOS_NODE_PID1=$!
${PAXOS_NODE} -myport=${NODE_PORT2} -ports=${ALL_PORTS} -N=10 -id=2 2>&1 & # /dev/null &
PAXOS_NODE_PID2=$!
${PAXOS_NODE} -myport=${NODE_PORT3} -ports=${ALL_PORTS} -N=10 -id=3 2>&1 & # /dev/null &
PAXOS_NODE_PID3=$!
${PAXOS_NODE} -myport=${NODE_PORT4} -ports=${ALL_PORTS} -N=10 -id=4 2>&1 & # /dev/null &
PAXOS_NODE_PID4=$!
${PAXOS_NODE} -myport=${NODE_PORT5} -ports=${ALL_PORTS} -N=10 -id=5 2>&1 & # /dev/null &
PAXOS_NODE_PID5=$!
${PAXOS_NODE} -myport=${NODE_PORT6} -ports=${ALL_PORTS} -N=10 -id=6 2>&1 & # /dev/null &
PAXOS_NODE_PID6=$!
${PAXOS_NODE} -myport=${NODE_PORT7} -ports=${ALL_PORTS} -N=10 -id=7 2>&1 & # /dev/null &
PAXOS_NODE_PID7=$!
${PAXOS_NODE} -myport=${NODE_PORT8} -ports=${ALL_PORTS} -N=10 -id=8 2>&1 & # /dev/null &
PAXOS_NODE_PID8=$!
${PAXOS_NODE} -myport=${NODE_PORT9} -ports=${ALL_PORTS} -N=10 -id=9 2>&1 & # /dev/null &
PAXOS_NODE_PID9=$!
sleep 5

${PAXOS_APP} -myport=8080 -nodePort=${NODE_PORT0} 2>&1 & # /dev/null &
CLIENT_NODE_PID0=$!
sleep 1
${PAXOS_APP} -myport=8081 -nodePort=${NODE_PORT1} 2>&1 & # /dev/null &
CLIENT_NODE_PID1=$!
sleep 1
${PAXOS_APP} -myport=8082 -nodePort=${NODE_PORT2} 2>&1 & # /dev/null &
CLIENT_NODE_PID2=$!
sleep 1
# ${PAXOS_APP} -myport=8083 -nodePort=${NODE_PORT3} 2>&1 & # /dev/null &
# sleep 3
# ${PAXOS_APP} -myport=8084 -nodePort=${NODE_PORT4} 2>&1 & # /dev/null &
# sleep 3
# ${PAXOS_APP} -myport=8085 -nodePort=${NODE_PORT5} 2>&1 & # /dev/null &
# sleep 3

# KILLPORT="${NODE_PORT6},${NODE_PORT7},${NODE_PORT8},${NODE_PORT9}"
# echo ${KILLPORT}
sleep 5


# # Kill paxos node.

kill -9 ${PAXOS_NODE_PID5}
kill -9 ${PAXOS_NODE_PID6}
kill -9 ${PAXOS_NODE_PID7}
kill -9 ${PAXOS_NODE_PID8}
# 
# wait ${PAXOS_NODE_PID7} 2> /dev/null
# wait ${PAXOS_NODE_PID8} 2> /dev/null
# wait ${PAXOS_NODE_PID9} 2> /dev/null
