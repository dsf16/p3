var mouseDown = false;
var numRows = 40;
var numCols = 90;
var cells = {}

function refreshPixels() {
    if (!mouseDown) {
            $.get("refresh")
        .done(function(data) {
            $.each(JSON.parse(data), function(key, value) {
                $(cells[key]).css({background: '#' + value});
            });
        });
        $.get("deadNode")
        .done(function(dead) {
            if (dead=="true") {
                alert("This node is dead.");
            }
        });
    }
}

function requestConsensus(cell) {
    markerColor = $('#select-color').val();
    
    // just send hex without hashtag symbol in get request
    colorStrip = markerColor.substr(1);
    row_col = cell.id.split(",")
    $(cell).css({background: markerColor});
	$.get("", {row: row_col[0], col: row_col[1], draw: colorStrip})
}

$(document).ready(function(){
    // set "pixel" ids
    for(var x = 0; x < numRows; x++) {
        for(var y = 0; y < numCols; y++) {
            var cell = $("<div class='cell'></div>");
            cell.attr("id", x + "," + y);
            cell.appendTo('#container');
            cells[x + "," + y] = cell;
        }
    }

    $('.cell').mousedown(function(event) {
        event.preventDefault();
        
        // if mousedown on any cell - set mouseDown to true
        mouseDown = true;
        
        requestConsensus(this);
    });

    $(document).mousedown(function() {
    }).mouseup(function() {
        // if mouseup anywhere - set mouseDown to false
        mouseDown = false;
    });

    $(".cell").mouseover(function(){
        if (mouseDown) {
            requestConsensus(this);
        }
    });

    refreshPixels();
    window.setInterval(refreshPixels, 500);
});