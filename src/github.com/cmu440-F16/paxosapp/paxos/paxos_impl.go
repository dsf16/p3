package paxos

import (
	"encoding/json"
	"errors"
	"github.com/cmu440-F16/paxosapp/rpc/paxosrpc"
	"net"
	"net/http"
	"net/rpc"
	"sync"
	"time"
	"fmt"
)

type db struct {
	sync.RWMutex
	kvStore map[string]interface{}
}

var PROPOSE_TIMEOUT = 15 * time.Second

type status struct {
	sync.RWMutex
	Np int         // highest n seen
	Nk int         // highest n accepted
	Ak interface{} // value associated with highest n accepted, of the same key
}

type proposalArg struct {
	sync.RWMutex
	Nk    int
	Vk    interface{}
	count int
}

type paxosNode struct {
	sync.RWMutex // mutex associated with keyStatus
	srvId        int
	clientMap    map[int]*rpc.Client
	ringLen      int

	storage   *db
	keyStatus map[string]*status

	proposalNumber map[string]int
}

type catchup struct {
	KvStore   map[string]interface{}
	KeyStatus map[string]*status
}

var NumRetries int

// Desc:
// NewPaxosNode creates a new PaxosNode. This function should return only when
// all nodes have joined the ring, and should return a non-nil error if this node
// could not be started in spite of dialing any other nodes numRetries times.
//
// Params:
// myHostPort: the hostport string of this new node. We use tcp in this project.
//			   	Note: Please listen to this port rather than hostMap[srvId]
// hostMap: a map from all node IDs to their hostports.
//				Note: Please connect to hostMap[srvId] rather than myHostPort
//				when this node try to make rpc call to itself.
// numNodes: the number of nodes in the ring
// numRetries: if we can't connect with some nodes in hostMap after numRetries attempts, an error should be returned
// replace: a flag which indicates whether this node is a replacement for a node which failed.
func NewPaxosNode(myHostPort string, hostMap map[int]string, numNodes, srvId, numRetries int, replace bool) (PaxosNode, error) {
	fmt.Printf("Initializing paxo on port:%v, ring size:%v\n", myHostPort, numNodes)
	NumRetries = numRetries
	var initDB = new(db)
	initDB.kvStore = make(map[string]interface{})
	pn := paxosNode{
		srvId:     srvId,
		clientMap: make(map[int]*rpc.Client),
		ringLen:   numNodes,

		storage:   initDB,
		keyStatus: make(map[string]*status),

		proposalNumber: make(map[string]int),
	}
	listener, err := net.Listen("tcp", myHostPort)
	if err != nil {
		return nil, err
	}
	err = rpc.RegisterName("PaxosNode", paxosrpc.Wrap(&pn))
	if err != nil {
		return nil, err
	}
	rpc.HandleHTTP()

	go http.Serve(listener, nil)

	// dial all nodes in hostMap

	for nodeSrvId, nodeHostPort := range hostMap {

		cli, err := rpc.DialHTTP("tcp", nodeHostPort)
		retries := 0
		tick := time.Tick(time.Duration(time.Second))

		// retry at most numRetries times
		for err != nil && retries < numRetries {
			cli, err = rpc.DialHTTP("tcp", nodeHostPort)
			retries++
			<-tick
		}

		if err != nil {
			// still can't connect after numRetries attempts
			fmt.Printf("Node %v failed to connect to node %v after %v retries\n", myHostPort, nodeHostPort, numRetries)
			return nil, err
		} else {
			pn.clientMap[nodeSrvId] = cli

			if replace && srvId != nodeSrvId {
				// notify all other servers if there is a replacement node
				replaceServerArgs := paxosrpc.ReplaceServerArgs{
					SrvID:    srvId,
					Hostport: hostMap[srvId],
				}
				var replaceServerReply paxosrpc.ReplaceServerReply
				cli.Call("PaxosNode.RecvReplaceServer", replaceServerArgs, &replaceServerReply)
			}
		}
	}
	if replace {
		replaceCatchupArgs := paxosrpc.ReplaceCatchupArgs{}
		var replaceCatchupReply paxosrpc.ReplaceCatchupReply
		for nodeSrvId, cli := range pn.clientMap {
			if nodeSrvId != srvId {
				cli.Call("PaxosNode.RecvReplaceCatchup", replaceCatchupArgs, &replaceCatchupReply)
				break
			}
		}
		var replaceReply catchup
		err := json.Unmarshal(replaceCatchupReply.Data, &replaceReply)
		if err != nil {
			return nil, err
		}

		// catchup data on replacement node
		for k, v := range replaceReply.KeyStatus {
			pn.keyStatus[k] = v
		}
		for k, v := range replaceReply.KvStore {
			initDB.kvStore[k] = v
		}
		pn.storage = initDB

	}
	fmt.Printf("New paxonode created successfully on port %v\n", myHostPort)
	return &pn, nil
}

// Desc:
// GetNextProposalNumber generates a proposal number which will be passed to
// Propose. Proposal numbers should not repeat for a key, and for a particular
// <node, key> pair, they should be strictly increasing.
//
// Params:
// args: the key to propose
// reply: the next proposal number for the given key
func (pn *paxosNode) GetNextProposalNumber(args *paxosrpc.ProposalNumberArgs, reply *paxosrpc.ProposalNumberReply) error {

	pn.Lock()
	kStatus, ok := pn.keyStatus[args.Key]
	if !ok {
		pn.Unlock()
		reply.N = pn.srvId

		return nil
	}
	kStatus.Lock()
	pn.Unlock()
	Np := kStatus.Np
	reply.N = (1+Np/pn.ringLen)*pn.ringLen + pn.srvId
	kStatus.Unlock()
	return nil
}

// Desc:
// Propose initializes proposing a value for a key, and replies with the
// value that was committed for that key. Propose should not return until
// a value has been committed, or PROPOSE_TIMEOUT seconds have passed.
//
// Params:
// args: the key, value pair to propose together with the proposal number returned by GetNextProposalNumber
// reply: value that was actually committed for the given key
func (pn *paxosNode) Propose(args *paxosrpc.ProposeArgs, reply *paxosrpc.ProposeReply) error {
	tick := time.NewTicker(PROPOSE_TIMEOUT)
	n := args.N
	key := args.Key
	v := args.V

	//Propose
	prepareArg := paxosrpc.PrepareArgs{Key: key, N: n, RequesterId: pn.srvId}
	voteChan1 := make(chan int)
	voteChan2 := make(chan int)
	cur_proposal := proposalArg{Nk: -1, Vk: v, count: 0}
	for _, cli := range pn.clientMap {
		go func(cli *rpc.Client) {
			var prepareReply paxosrpc.PrepareReply
			cli.Call("PaxosNode.RecvPrepare", prepareArg, &prepareReply)

			if prepareReply.Status == paxosrpc.OK {
				// received Prepare_OK
				if prepareReply.N_a > cur_proposal.Nk {

					cur_proposal.Lock()
					cur_proposal.Nk = prepareReply.N_a
					cur_proposal.Vk = prepareReply.V_a
					if prepareReply.N_a == -1 && prepareReply.V_a == nil {
						cur_proposal.count++
					}
					cur_proposal.Unlock()
				}
				voteChan1 <- 1
			} else {
				// received Reject Prepare
				voteChan1 <- 0
			}
		}(cli)
	}

	votes := 0
	numNodes := pn.ringLen
	majority := (numNodes / 2) + 1

	// name this for loop so we can break later
Prepare:
	for {
		select {
		case <-tick.C:
			reply.NoMajority = true
			return errors.New("Propose timed out in prepare")
		case vote := <-voteChan1:
			if vote == 0 {
				return errors.New("Prepare received reject before reach majority")
			}
			votes += vote
			if votes >= majority {
				// move onto accept phase
				cur_proposal.Lock()
				break Prepare
			}
		}
	}

	var acceptArg paxosrpc.AcceptArgs
	if cur_proposal.count >= majority {
		cur_proposal.Vk = v
	}

	acceptArg = paxosrpc.AcceptArgs{Key: key, N: n, V: cur_proposal.Vk, RequesterId: pn.srvId}

	// Accept
	for _, cli := range pn.clientMap {
		go func(cli *rpc.Client) {
			var acceptReply paxosrpc.AcceptReply
			cli.Call("PaxosNode.RecvAccept", acceptArg, &acceptReply)

			if acceptReply.Status == paxosrpc.OK {
				voteChan2 <- 1
			} else {
				voteChan2 <- 0
			}
		}(cli)
	}
	votes = 0

Accept:
	for {
		select {
		case <-tick.C:
			reply.NoMajority = true
			return errors.New("Propose timed out in accept")
		case vote := <-voteChan2:
			if vote == 0 {
				return errors.New("Accept received reject before reach majority")
			}
			votes += vote
			if votes >= majority {
				break Accept
			}
		}
	}
	// Commit
	commitArg := paxosrpc.CommitArgs{Key: key, V: cur_proposal.Vk, RequesterId: pn.srvId}
	syncChan := make(chan bool)
	for _, cli := range pn.clientMap {
		go func(cli *rpc.Client) {
			var commitReply paxosrpc.CommitReply
			cli.Call("PaxosNode.RecvCommit", commitArg, &commitReply)
			syncChan <- true
		}(cli)
	}

	//block until all goroutines returns
	for i := 0; i < numNodes; i++ {
		select {
		case <-tick.C:
			return errors.New("Commit timed out")
		case <-syncChan:

		}
	}
	reply.V = cur_proposal.Vk
	reply.NoMajority = false
	cur_proposal.Unlock()
	return nil
}

// Desc:
// GetValue looks up the value for a key, and replies with the value or with
// the Status KeyNotFound.
//
// Params:
// args: the key to check
// reply: the value and status for this lookup of the given key
func (pn *paxosNode) GetValue(args *paxosrpc.GetValueArgs, reply *paxosrpc.GetValueReply) error {
	pn.storage.RLock()
	value, ok := pn.storage.kvStore[args.Key]
	pn.storage.RUnlock()
	if !ok {
		reply.Status = paxosrpc.KeyNotFound
	} else {
		reply.Status = paxosrpc.KeyFound
		reply.V = value
	}
	return nil
}

// Desc:
// Receive a Prepare message from another Paxos Node. The message contains
// the key whose value is being proposed by the node sending the prepare
// message. This function should respond with Status OK if the prepare is
// accepted and Reject otherwise.
//
// Params:
// args: the Prepare Message, you must include RequesterId when you call this API
// reply: the Prepare Reply Message
func (pn *paxosNode) RecvPrepare(args *paxosrpc.PrepareArgs, reply *paxosrpc.PrepareReply) error {

	key := args.Key
	n := args.N
	pn.Lock()
	kStatus, ok := pn.keyStatus[key]
	if !ok {
		kStatus = &status{Np: -1, Nk: -1, Ak: nil}
		pn.keyStatus[key] = kStatus
	}
	kStatus.Lock()
	pn.Unlock()
	Np := kStatus.Np
	Nk := kStatus.Nk
	Ak := kStatus.Ak

	if n > Np {
		kStatus.Np = n
		reply.Status = paxosrpc.OK
		reply.N_a = Nk
		reply.V_a = Ak
		
	} else {
		reply.Status = paxosrpc.Reject
		reply.N_a = -1
		reply.V_a = nil
		
	}

	kStatus.Unlock()
	return nil
}

// Desc:
// Receive an Accept message from another Paxos Node. The message contains
// the key whose value is being proposed by the node sending the accept
// message. This function should respond with Status OK if the prepare is
// accepted and Reject otherwise.
//
// Params:
// args: the Please Accept Message, you must include RequesterId when you call this API
// reply: the Accept Reply Message
func (pn *paxosNode) RecvAccept(args *paxosrpc.AcceptArgs, reply *paxosrpc.AcceptReply) error {
	key := args.Key
	n := args.N
	v := args.V
	pn.Lock()
	kStatus, ok := pn.keyStatus[key]
	if !ok {
		kStatus = &status{Np: -1, Nk: -1, Ak: nil}
		pn.keyStatus[key] = kStatus
	}
	kStatus.Lock()
	pn.Unlock()
	Np := kStatus.Np

	if n >= Np {
		kStatus.Nk = n
		kStatus.Ak = v
		reply.Status = paxosrpc.OK

	} else {
		reply.Status = paxosrpc.Reject

	}
	kStatus.Unlock()
	return nil
}

// Desc:
// Receive a Commit message from another Paxos Node. The message contains
// the key whose value was proposed by the node sending the commit
// message.
//
// Params:
// args: the Commit Message, you must include RequesterId when you call this API
// reply: the Commit Reply Message
func (pn *paxosNode) RecvCommit(args *paxosrpc.CommitArgs, reply *paxosrpc.CommitReply) error {
	key := args.Key
	v := args.V

	pn.storage.Lock()
	pn.storage.kvStore[key] = v

	pn.storage.Unlock()

	pn.Lock()
	kStatus, ok := pn.keyStatus[key]
	if ok {
		kStatus.Nk = -1
		kStatus.Ak = nil
	}
	pn.Unlock()

	return nil
}

// Desc:
// Notify another node of a replacement server which has started up. The
// message contains the Server ID of the node being replaced, and the
// hostport of the replacement node
//
// Params:
// args: the id and the hostport of the server being replaced
// reply: no use
func (pn *paxosNode) RecvReplaceServer(args *paxosrpc.ReplaceServerArgs, reply *paxosrpc.ReplaceServerReply) error {

	cli, err := rpc.DialHTTP("tcp", args.Hostport)

	if err != nil { // still can't connect after numRetries attempts
		return err
	} else {
		pn.clientMap[args.SrvID] = cli
	}

	return nil
}

// Desc:
// Request the value that was agreed upon for a particular round. A node
// receiving this message should reply with the data (as an array of bytes)
// needed to make the replacement server aware of the keys and values
// committed so far.
//
// Params:
// args: no use
// reply: a byte array containing necessary data used by replacement server to recover

//reply.Data contains mashalled struct catchup
func (pn *paxosNode) RecvReplaceCatchup(args *paxosrpc.ReplaceCatchupArgs, reply *paxosrpc.ReplaceCatchupReply) error {
	pn.storage.RLock()
	pn.RLock()

	info := catchup{
		KvStore:   pn.storage.kvStore,
		KeyStatus: pn.keyStatus,
	}
	pn.RUnlock()
	pn.storage.RUnlock()
	data, err := json.Marshal(info)
	if err != nil {
		return err
	}
	reply.Data = data
	return nil
}
